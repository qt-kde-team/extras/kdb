Source: kdb
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Pino Toscano <pino@debian.org>
Build-Depends: cmake (>= 3.0),
               debhelper-compat (= 13),
               default-libmysqlclient-dev,
               extra-cmake-modules (>= 1.8.0),
               libicu-dev,
               libkf5coreaddons-dev (>= 5.16.0),
               libkf5i18n-dev (>= 5.16.0),
               libpq-dev,
               libsqlite3-dev,
               pkgconf,
               pkg-kde-tools (>= 0.15.16),
               postgresql-server-dev-all,
               python3:any,
               qtbase5-dev (>= 5.4.0),
               qttools5-dev,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://community.kde.org/KDb
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/kdb.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/kdb

Package: libkdb3-4abi1
X-Debian-ABI: 1
X-CMake-Target: KDb
Architecture: any
Multi-Arch: same
Depends: libkdb-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: database connectivity and creation framework -- shared library
 KDb is a database connectivity and creation framework, consisted of a
 general-purpose C++ Qt library and set of plugins delivering support
 for various database vendors.
 .
 This package contains the shared library.
 .
 This package is part of the Calligra Suite.

Package: libkdb-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: data files for KDb
 KDb is a database connectivity and creation framework, consisted of a
 general-purpose C++ Qt library and set of plugins delivering support
 for various database vendors.
 .
 This package contains the architecture independent data files for the
 KDb library and its drivers.
 .
 This package is part of the Calligra Suite.

Package: libkdb3-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libkdb3-4abi1 (= ${binary:Version}),
         qtbase5-dev (>= 5.4.0),
         libkf5coreaddons-dev (>= 5.16.0),
         ${misc:Depends},
Description: development files for KDb
 KDb is a database connectivity and creation framework, consisted of a
 general-purpose C++ Qt library and set of plugins delivering support
 for various database vendors.
 .
 This package contains the development files for KDb.
 .
 This package is part of the Calligra Suite.

Package: libkdb3-driver-sqlite
Architecture: any
Depends: libkdb3-4abi1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: SQLite driver for KDb
 KDb is a database connectivity and creation framework, consisted of a
 general-purpose C++ Qt library and set of plugins delivering support
 for various database vendors.
 .
 This package contains the support for SQLite in KDb.
 .
 This package is part of the Calligra Suite.

Package: libkdb3-driver-mysql
Architecture: any
Multi-Arch: same
Depends: libkdb3-4abi1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: MySQL driver for KDb
 KDb is a database connectivity and creation framework, consisted of a
 general-purpose C++ Qt library and set of plugins delivering support
 for various database vendors.
 .
 This package contains the support for MySQL in KDb.
 .
 This package is part of the Calligra Suite.

Package: libkdb3-driver-postgresql
Architecture: any
Multi-Arch: same
Depends: libkdb3-4abi1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: PostgreSQL driver for KDb
 KDb is a database connectivity and creation framework, consisted of a
 general-purpose C++ Qt library and set of plugins delivering support
 for various database vendors.
 .
 This package contains the support for PostgreSQL in KDb.
 .
 This package is part of the Calligra Suite.
